package fer.projekt.threads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncHttpPostTask extends AsyncTask<String, Void, Integer> {
	private static final String TAG = AsyncHttpPostTask.class.getSimpleName();
	private String server;
	private String url;
	private Integer userID;
	
	public AsyncHttpPostTask(final String server) {
		this.server = server;
	}
	
	private StringBuilder inputStreamToString(InputStream is) {
	    String line = "";
	    StringBuilder total = new StringBuilder();
	    
	    // Wrap a BufferedReader around the InputStream
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is));

	    // Read response until the end
	    try {
			while ((line = rd.readLine()) != null) { 
			    total.append(line); 
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    // Return full string
	    return total;
	}


	@Override
	protected Integer doInBackground(String... args) {
		
		url = this.server + "save/id=" + args[0] + 
							"&username="+ args[1] + 
							"&latitude=" + args[2] + 
							"&longitude=" + args[3];
		
		Log.d(TAG, url);
		
		HttpClient http = new DefaultHttpClient();
		HttpPost method = new HttpPost(url);
		
		try {
			HttpResponse response = http.execute(method);
			userID = Integer.parseInt(inputStreamToString(response.getEntity().getContent()).toString());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(NumberFormatException e) {
			e.printStackTrace();
		}
		
		return userID;
	}

}
