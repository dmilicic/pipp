package fer.projekt.dialogs;

import fer.projekt.geotest.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.widget.EditText;

public class DialogFactory {

	public static void enterUsernameDialog(final Activity activity) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity); 	
		builder.setTitle("New user");
		builder.setMessage("Enter your username:");
		
		final EditText input = new EditText(activity);
		input.setSingleLine();
		input.setText("");
		builder.setView(input);
		
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString(activity.getString(R.string.username), input.getText().toString());
				editor.commit();
			}
		});
		
		builder.show();
	}

}
