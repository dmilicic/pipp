package fer.projekt.geotest;

import java.util.concurrent.ExecutionException;

import fer.projekt.dialogs.DialogFactory;
import fer.projekt.threads.AsyncHttpPostTask;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	public static LocationManager locationManager;
	
	private final LocationListener listener = new LocationListener() {
		
		public void onLocationChanged(Location location) {
			TextView longitude = (TextView) findViewById(R.id.longitude);
			TextView latitude = (TextView) findViewById(R.id.latitude);
			
			String param1 = Double.toString(location.getLongitude());
			String param2 = Double.toString(location.getLatitude());
					
			longitude.setText(param1);
			latitude.setText(param2);
			
			SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
			String user = sharedPref.getString(getString(R.string.username), null);
			Integer userID = sharedPref.getInt(getString(R.string.username_id), 0);
			
			AsyncHttpPostTask asyncTask = new AsyncHttpPostTask("http://192.168.43.176:8000/android/");
			asyncTask = (AsyncHttpPostTask) asyncTask.execute(userID.toString(), user, param1, param2);
			
			try {
				SharedPreferences.Editor editor = sharedPref.edit();
				userID = asyncTask.get();
				Log.d("main, id:", userID.toString());
				editor.putInt(getString(R.string.username_id), userID);
				editor.commit();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        
        if(!gpsEnabled) {
        	Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        	startActivity(settingsIntent);
        }
        
        checkForUsername();
    }
    
    public void checkForUsername() {
    	
    	SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
    	String username = sharedPref.getString(getString(R.string.username), null);
    	
    	if (username == null) {
    		DialogFactory.enterUsernameDialog(this);
    	}
    }
    
    public void sendMessage(View view) {
    	EditText textMinutes = (EditText) findViewById(R.id.minutes);
    	EditText textSeconds = (EditText) findViewById(R.id.seconds);
    	
    	Integer minutes = null, seconds = null;
    	String mins = textMinutes.getText().toString();
    	String sec = textSeconds.getText().toString();
    	
    	Log.d("minutes", mins);
    	Log.d("seconds", sec);
    	
    	try {
        	minutes = Integer.parseInt(mins);
        	seconds = Integer.parseInt(sec);
	    	
    	} catch (NumberFormatException e) {
    		if(mins.equals(""))
    			minutes = 0;
    		if(sec.equals(""))
    			seconds = 0;
    	}
    	
//    	Log.d("seconds", seconds.toString());
//    	Log.d("minutes", minutes.toString());
    	
    	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, seconds * 1000 + minutes * 60 * 1000, 0, listener);
    }
}
